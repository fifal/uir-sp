package cz.zcu.kiv.uir.fjani.algorithms;

import cz.zcu.kiv.uir.fjani.model.Vector;
import cz.zcu.kiv.uir.fjani.util.Util;
import javafx.scene.image.Image;


/**
 * Created by Filip Jani on 2. 5. 2016.
 */
public class BagOfWords {
    public BagOfWords() {
    }

    /**
     * Vytvoří příznakový vektor a uloží ho do paměti
     * @param image  image obrázek pro který se vektor generuje
     * @param number number číslo pro který se vektor generuje
     */
    public void learn(Image image, int number) {
        int[][] array = Util.getArrayFromImage(image);
        int[] vector = getVector(array);
        if (!Util.isInVectorList(vector)) {
            Util.addVector(new Vector(number, vector));
        }
        /*try {
            Util.saveVectorsToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Vrací příznakový vektor
     * @param array pole nul a jedniček
     * @return int[] příznakový vektor
     */
    public static int[] getVector(int[][]array){
        int[] vector = new int[128];
        for (int i = 0; i < array.length; i++) {
            int totalWordCount = Integer.MIN_VALUE;
            int wordCount = 0;
            for (int j = 0; j < array[0].length; j++) {
                if(array[i][j]==1){
                    wordCount++;
                }
                else{
                    if (wordCount > totalWordCount){
                        totalWordCount = wordCount;
                        wordCount = 0;
                    }
                }
            }
            if(totalWordCount != Integer.MIN_VALUE){
                vector[totalWordCount]++;
            }
        }
        return vector;
    }
}
