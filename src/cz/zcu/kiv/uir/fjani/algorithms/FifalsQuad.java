package cz.zcu.kiv.uir.fjani.algorithms;

import cz.zcu.kiv.uir.fjani.model.Vector;
import cz.zcu.kiv.uir.fjani.util.Util;
import javafx.scene.image.Image;

/**
 * Created by Filip Jani on 3. 5. 2016.
 */
public class FifalsQuad {

    public FifalsQuad() {
    }

    /**
     * Vyytvoří příznakový vektor z obrázku a vloží tento vektor do souboru
     *
     * @param image  image obrázek pro který se vektor generuje
     * @param number number číslo pro který se vektor generuje
     */
    public void learn(Image image, int number) {
        int[][] arrray = Util.getArrayFromImage(image);
        arrray = Util.doZhangSuenThinning(arrray);
        int[] vector = getVector(arrray);
        if (!Util.isInVectorList(vector)) {
            Util.addVector(new Vector(number, vector));
        }
        try{
            Util.saveVectorsToFile();
        }
        catch (Exception ex){

        }
    }


    /**
     * Vrací příznakový vektor pro dané pole
     *
     * @param array pole nul a jedniček
     * @return vector příznakový vektor
     */
    public static int[] getVector(int[][] array) {
        int[] vector = new int[32];
        int k = 0;
        for (int i = 0; i < array.length-4; i=i+4) {
            int count = 0;
            for (int j = 0; j < array[0].length-4; j=j+4) {
                for (int l = 0; l < 4; l++) {
                    for (int m = 0; m < 4; m++) {
                        count += array[i][j];
                        count += array[i+l][j];
                        count += array[i+l][j+m];
                        count += array[i][j+m];
                    }
                }
            }
            vector[k] = count;
            k++;
        }
        return vector;
    }

}
