package cz.zcu.kiv.uir.fjani.algorithms;

import cz.zcu.kiv.uir.fjani.config.Config;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;


public class KNN {
    /**
     * Vrací výsledné číslo, vezme počet nátrénovaných množin, seřadí čísla v listu a vybere to s nejvyšší četností
     * nebo číslo, které má nejbližší vzdálenost k našemu hádanému číslu.
     * @param map list s dvojicí číslo - vzdálenost
     * @return
     */
    public static int getResult(Map<Double, Integer> map){
        ArrayList<Integer> numbers = new ArrayList<>();
        TreeMap<Double, Integer> sorted = new TreeMap<>(map);
        int count = 0;
        for (int i = 0; i < sorted.size(); i++) {
            if (count < Config.NUMBERS_LOADED) {
                numbers.add((int) sorted.values().toArray()[i]);
                count++;
            }
        }

        //Určíme které číslo se vyskytuje v poli nejčastěji a vybereme ho jako uhodnuté číslos
        int number = -1;
        int numberCount = 0;
        for (int i = 0; i < numbers.size(); i++) {
            int tempCount = numberOfOccurrence(numbers, numbers.get(i));
            if (tempCount > numberCount) {
                numberCount = tempCount;
                number = numbers.get(i);
            }
        }

        return number;
    }

    /**
     * Vrací počet výskytů pro dané číslo v arraylistu
     * @param numberList arraylist čísel
     * @param number hledané číslo
     * @return četnost výskytu čísla
     */
    private static int numberOfOccurrence(ArrayList<Integer> numberList, int number) {
        int count = 0;
        for (Integer i : numberList) {
            if (i == number) {
                count++;
            }
        }
        return count;
    }
}
