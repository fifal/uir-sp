package cz.zcu.kiv.uir.fjani.model;

import java.util.Arrays;

/**
 * Created by Filip Jani on 27. 4. 2016.
 *
 */
public class Vector {
    private int number;
    private int[] vector;

    public Vector(int number, int[] vector){
        this.number = number;
        this.vector = vector;
    }

    public int getNumber() {
        return number;
    }

    public int[] getVector() {
        return vector;
    }

    @Override
    public String toString(){
        int total = 0;
        for (int i = 0; i < vector.length; i++) {
            total+= vector[i];
        }
        return number + ":" +total+":"+ Arrays.toString(vector);
    }
}
