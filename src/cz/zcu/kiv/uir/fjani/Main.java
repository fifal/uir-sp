package cz.zcu.kiv.uir.fjani;

import cz.zcu.kiv.uir.fjani.controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("layouts/layout.fxml")
        );
        Parent root = loader.load();
        primaryStage.setTitle("UIR Semestrální práce - Filip Jani - A14B0269P");
        primaryStage.setScene(new Scene(root, 1000, 600));
        primaryStage.show();
        primaryStage.setResizable(false);
        Controller cont = loader.getController();
        cont.init();
    }


    public static void main(String[] args) {
        if (args.length == 1) {
            launch(args);
        } else if(args.length == 5){
            launch(args);
        }
        else{
            System.out.println("Špatný počet parametrů.");
            System.out.println("Program spouštějte: ");
            System.out.println("S jedním parametrem <název_modelu>");
            System.out.println("S pěti parametry <trénovací_množina> <testovací_množina> <param_algoritmus>" +
                    " <klasif_algoritmus> <název_modelu>");
        }
    }
}
