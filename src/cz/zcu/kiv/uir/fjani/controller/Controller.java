package cz.zcu.kiv.uir.fjani.controller;

import cz.zcu.kiv.uir.fjani.algorithms.BagOfWords;
import cz.zcu.kiv.uir.fjani.algorithms.FifalsQuad;
import cz.zcu.kiv.uir.fjani.model.Vector;
import cz.zcu.kiv.uir.fjani.util.Util;
import cz.zcu.kiv.uir.fjani.algorithms.Fifals;
import cz.zcu.kiv.uir.fjani.config.Config;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.stage.DirectoryChooser;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * FXML Controller
 */
public class Controller {
    @FXML
    public Canvas canvas;
    private GraphicsContext gc;

    private Fifals fifals;
    private BagOfWords bagOfWords;
    private FifalsQuad fifalsQuad;

    @FXML
    public Label guessedNumber;

    @FXML
    public StackPane stackPane;
    @FXML
    public BorderPane borderPane;

    @FXML
    public ChoiceBox<String> symptomAlgorithm;
    @FXML
    public ChoiceBox<String> classAlgorithm;


    @FXML
    public Button cleanBtn;

    @FXML
    public Button processBtn;
    @FXML
    public Button learnBtn;

    @FXML
    public Button trainBtn;
    @FXML
    public TextField trainText;

    @FXML
    public Button testBtn;
    @FXML
    public TextField testText;

    @FXML
    public VBox infoVBox;

    @FXML
    public Button startTestBtn;

    @FXML
    public TextArea infoArea;
   /* @FXML
    private TextField numberText;*/

    /**
     * Initialization of App components
     */
    public void init() {
        //Css and sizes
        stackPane.setStyle("-fx-background-color: #e1e1e1");
        borderPane.setStyle("-fx-background-color: lightgray");
        infoVBox.setStyle("-fx-background-color: #e1e1e1");
        processBtn.setStyle("-fx-background-color: linear-gradient(#97ed63, #7bce49);");
        processBtn.setDisable(true);
        learnBtn.setDisable(true);
        startTestBtn.setDisable(true);
        canvas.setWidth(Config.CAVNVAS_SIZE);
        canvas.setHeight(Config.CAVNVAS_SIZE);
        guessedNumber.setVisible(false);
        guessedNumber.setTextFill(Paint.valueOf("#FF0000"));

        //Canvas initialization, number frame render
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Paint.valueOf("FFF"));
        gc.fillRect(0, 0, Config.CAVNVAS_SIZE, Config.CAVNVAS_SIZE);

        //Canvas events, necessary for drawing
        canvas.setOnMouseDragged(event -> {
            gc.lineTo(event.getX(), event.getY());
            gc.setLineWidth(Config.BRUSH_SIZE);
            gc.setLineJoin(StrokeLineJoin.ROUND);
            gc.setLineCap(StrokeLineCap.ROUND);
            gc.setStroke(Paint.valueOf("000"));
            gc.stroke();
        });
        canvas.setOnMousePressed(event -> {
            gc.beginPath();
            gc.moveTo(event.getX(), event.getY());
        });
        canvas.setOnMouseReleased(event -> gc.closePath());

        symptomAlgorithm.getItems().add("Fifals");
        symptomAlgorithm.getItems().add("BoW");
        symptomAlgorithm.getItems().add("FifalsQuad");
        symptomAlgorithm.setValue("Fifals");

        symptomAlgorithm.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                learnNumberChoice(newValue);
            }
        });

        classAlgorithm.getItems().add("KNN");
        classAlgorithm.getItems().add("SmallestDistance");
        classAlgorithm.setValue("KNN");

        fifals = new Fifals();
        bagOfWords = new BagOfWords();
        fifalsQuad = new FifalsQuad();
    }

    /**
     * Method process drawn image
     */
    @FXML
    private void processImg() {
        if (!trainText.getText().equals("")) {
            guessedNumber.setVisible(true);
            guessedNumber.setText("Uhadnuto: " + Util.recognize(getImg(), symptomAlgorithm.getValue(), classAlgorithm.getValue()));
        }
    }

    /**
     * Resizes image from canvas to 128x128
     *
     * @return resized image
     */
    private Image getImg() {
        WritableImage wim = new WritableImage(Config.CAVNVAS_SIZE, Config.CAVNVAS_SIZE);
        canvas.snapshot(null, wim);
        BufferedImage originalImage = SwingFXUtils.fromFXImage(wim, null);
        return SwingFXUtils.toFXImage(resizeImage(originalImage), null);
    }

    @FXML
    private void clearCanvas() {
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Paint.valueOf("FFF"));
        gc.fillRect(0, 0, Config.CAVNVAS_SIZE, Config.CAVNVAS_SIZE);
    }

    @FXML
    private void learnNumberBtn() {
        if (!trainText.getText().equals("")) {
            learnNumber(trainText.getText(), symptomAlgorithm.getValue());
        }
    }

    private void learnNumberChoice(Number newValue) {
        learnNumber(trainText.getText(), symptomAlgorithm.getItems().get((int) newValue));
    }

    /**
     * Learns number from data folder
     */
    private void learnNumber(String path, String parAlg) {
        Util.clearVectorList();
        int totalCount = 0;
        File directory = new File(path);
        File[] directories = directory.listFiles();
        Config.NUMBERS_LOADED = directories.length;
        for (File file : directories) {
            for (int number = 0; number < 10; number++) {
                File numberDirectory = new File(file.getAbsolutePath() + "/" + number);
                File[] numberFiles = numberDirectory.listFiles();
                for (File fajl : numberFiles) {
                    File numberFile = new File(fajl.getAbsolutePath());
                    if (numberFile.exists()) {
                        StringBuilder stringBuilder = new StringBuilder();
                        try {
                            int i = 0;
                            BufferedReader bufferedReader = new BufferedReader(new FileReader(numberFile));
                            String line = bufferedReader.readLine();
                            while (line != null) {
                                if (i > 3) {
                                    stringBuilder.append(line);
                                }
                                line = bufferedReader.readLine();
                                i++;
                            }
                            bufferedReader.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String[] parsedSplit = stringBuilder.toString().split(" ");
                        BufferedImage bi = new BufferedImage(128, 128, BufferedImage.TYPE_INT_BGR);
                        try {
                            for (int i = 0; i < 128; i++) {
                                for (int j = 0; j < 128; j++) {
                                    if (Integer.parseInt(parsedSplit[i * 128 + j]) < Config.TOLERATION) {
                                        bi.setRGB(j, i, 0);
                                    } else {
                                        bi.setRGB(j, i, Color.WHITE.getRGB());
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            break;
                        }
                        BufferedImage resized = resizeImage(bi);
                        switch (parAlg) {
                            case "Fifals":
                                fifals.learn(SwingFXUtils.toFXImage(resized, null), number);
                                break;
                            case "BoW":
                                bagOfWords.learn(SwingFXUtils.toFXImage(bi, null), number);
                                break;
                            case "FifalsQuad":
                                fifalsQuad.learn(SwingFXUtils.toFXImage(resized, null), number);
                                break;
                        }

                    }
                }

            }
        }
        processBtn.setDisable(false);
        if (!testText.getText().equals("")) {
            startTestBtn.setDisable(false);
        }
        try {
            Util.saveVectorsToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crops image, and resizes it to size [128,128]
     *
     * @param originalImage BufferedImage
     * @return BufferedImage
     */
    private BufferedImage resizeImage(BufferedImage originalImage) {
        BufferedImage resizedImage = new BufferedImage(128, 128, originalImage.getType());
        Point p1 = new Point();
        Point p2 = new Point();
        for (int i = 0; i < originalImage.getHeight() - 1; i++) {
            for (int j = 0; j < originalImage.getWidth() - 1; j++) {
                int rgb = Util.convertRGB(originalImage.getRGB(i, j));
                if (rgb < 180) {
                    p2.x = i;
                    break;
                }
            }
        }
        for (int i = 0; i < originalImage.getHeight() - 1; i++) {
            for (int j = 0; j < originalImage.getWidth() - 1; j++) {
                int rgb = Util.convertRGB(originalImage.getRGB(j, i));
                if (rgb < 180) {
                    p2.y = i;
                    break;
                }
            }
        }
        for (int i = originalImage.getHeight() - 1; i > 0; i--) {
            for (int j = originalImage.getWidth() - 1; j > 0; j--) {
                int rgb = Util.convertRGB(originalImage.getRGB(i, j));
                if (rgb < 180) {
                    p1.x = i;
                    break;
                }
            }
        }
        for (int i = originalImage.getHeight() - 1; i > 0; i--) {
            for (int j = originalImage.getWidth() - 1; j > 0; j--) {
                int rgb = Util.convertRGB(originalImage.getRGB(j, i));
                if (rgb < 180) {
                    p1.y = i;
                    break;
                }
            }
        }
        Graphics2D g = resizedImage.createGraphics();
        try {
            BufferedImage subImage = originalImage.getSubimage(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
            g.drawImage(subImage, 0, 0, 128, 128, 0, 0, subImage.getWidth(), subImage.getHeight(), null);
            g.dispose();
        } catch (Exception ex) {
            g.drawImage(originalImage, 0, 0, 128, 128, 0, 0, originalImage.getWidth(), originalImage.getHeight(), null);
            g.dispose();
        }

        return resizedImage;
    }

    @FXML
    public void selectTrainData() {
        DirectoryChooser dc = new DirectoryChooser();
        File home = new File(".");
        dc.setInitialDirectory(home);
        dc.setTitle("Vyberte složku s trénovacími daty");
        File chosenFile = dc.showDialog(null);
        trainText.setText(chosenFile.getAbsolutePath());
        if (!chosenFile.getAbsoluteFile().equals("")) {
            learnBtn.setDisable(false);
        }
    }

    @FXML
    public void selectTestData() {
        DirectoryChooser dc = new DirectoryChooser();
        File home = new File(".");
        dc.setInitialDirectory(home);
        dc.setTitle("Vyberte složku s testovacími daty");
        File chosenFile = dc.showDialog(null);
        testText.setText(chosenFile.getAbsolutePath());
        if (!chosenFile.getAbsoluteFile().equals("")) {
            processBtn.setDisable(false);
            startTestBtn.setDisable(false);
        }
    }

    @FXML
    public void testData() {
        infoArea.clear();
        double recognized = 0;
        double faults = 0;
        double total = 0;
        infoArea.setText(symptomAlgorithm.getValue() + " - " + classAlgorithm.getValue() + "\n");
        File directory = new File(testText.getText());
        if (directory.exists()) {
            File[] directories = directory.listFiles();
            for (File file : directories) {
                infoArea.setText(infoArea.getText() + file.getName() + "\n");
                for (int number = 0; number < 10; number++) {
                    File numberFile = new File(file.getAbsolutePath() + "/" + number + "/" + number + ".pgm");
                    if (!numberFile.exists()) {
                        numberFile = new File(file.getAbsolutePath() + "/" + number + "/c" + number + ".pgm");
                    }
                    if (numberFile.exists()) {
                        StringBuilder stringBuilder = new StringBuilder();
                        try {
                            int i = 0;
                            BufferedReader bufferedReader = new BufferedReader(new FileReader(numberFile));
                            String line = bufferedReader.readLine();
                            while (line != null) {
                                if (i > 3) {
                                    stringBuilder.append(line);
                                }
                                line = bufferedReader.readLine();
                                i++;
                            }
                            bufferedReader.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String[] parsedSplit = stringBuilder.toString().split(" ");
                        BufferedImage bi = new BufferedImage(128, 128, BufferedImage.TYPE_INT_BGR);
                        try {
                            for (int i = 0; i < 128; i++) {
                                for (int j = 0; j < 128; j++) {
                                    if (Integer.parseInt(parsedSplit[i * 128 + j]) < Config.TOLERATION) {
                                        bi.setRGB(j, i, 0);
                                    } else {
                                        bi.setRGB(j, i, Color.WHITE.getRGB());
                                    }
                                }
                            }
                            if (!symptomAlgorithm.getValue().equals("BoW")) {
                                BufferedImage resized = resizeImage(bi);
                                Image image = SwingFXUtils.toFXImage(resized, null);
                                int guessed = Integer.parseInt(Util.recognize(image, symptomAlgorithm.getValue(), classAlgorithm.getValue()));
                                if (guessed == number) {
                                    recognized++;
                                } else {
                                    faults++;
                                }
                                total++;
                                infoArea.setText(infoArea.getText() + "mělo být číslo: " + number + " uhodnuto: " + guessed + "\n");
                            } else {
                                Image image = SwingFXUtils.toFXImage(bi, null);
                                int guessed = Integer.parseInt(Util.recognize(image, symptomAlgorithm.getValue(), classAlgorithm.getValue()));
                                if (guessed == number) {
                                    recognized++;
                                } else {
                                    faults++;
                                }
                                total++;
                                infoArea.setText(infoArea.getText() + "mělo být číslo: " + number + " uhodnuto: " + guessed + "\n");
                            }
                        } catch (Exception ex) {
                            break;
                        }
                    }
                }
                infoArea.setText(infoArea.getText() + "\n");
            }
        }
        double acc = (recognized / total) * 100;
        infoArea.setText(infoArea.getText() + "přesnost: " + acc + "%");
    }
}
