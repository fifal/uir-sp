package cz.zcu.kiv.uir.fjani.config;

/**
 * Created by Filip Jani on 9. 4. 2016.
 */
public final class Config {
    public static int BRUSH_SIZE = 8;
    public static int CAVNVAS_SIZE = 512;
    public static int TOLERATION = 235;

    public static int NUMBERS_LOADED;
}
